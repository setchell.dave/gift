#!/bin/sh
: ${SH_BIN:=@bindir@}
: ${SH_SHARE:="@shlibdir@"}
test "$SH_BIN" != ${SH_BIN#@} && SH_BIN=$PWD
test $SH_SHARE != ${SH_SHARE#@} && SH_SHARE=./shlib
. $SH_SHARE/.func.sh
. $SH_SHARE/.msg.sh

##_DESIGN initial
# gift.sh essentially aids in using git branches for issues, features, and todos
# the idea is that each of those gets a branch such that tracking the changes
# and developments of such things remains in the repository and their
# "locality of reference" remains as tight as possible.
# System should assist in finding, selecting, and adding to those branches
##_

_git () { # just runs git. dumb?
    git "$@"
}

_archive () {
    _git tag archive/${gift_branch}    
    _git push origin archive/${gift_branch}
}

_current () { # branch -> bool
    test "$(_git rev-parse --verify ${1:-master} 2>/dev/null)" = \
        "$(_git rev-parse --verify origin/${1:-master} 2>/dev/null)"
}

_gift_branches () { # () -> branch list
    _git branch -a --sort=-committerdate \
        | sed -n 's#.*origin/\([A-Z]\+/.*\)#\1#p' | grep '\(ISSUE\|TODO\|FEATURE\)' 
}

_gift_file () { # gift-branch -> gift-file
	test -z "$1" && _error must provide gift branch
	echo .gift/$(echo ${1} | tr '/' '.')
}

_list () {
    _git fetch origin
    _gift_branches | while read check_branch
    do
		_current $check_branch && echo yes || echo no
		_git rev-parse --verify "$check_branch" 2>/dev/null 1>&2 \
			|| check_branch=origin/${check_branch}
		_git merge-base --is-ancestor $base_branch ${check_branch} \
			&& echo yes || echo no
		_git show --format='[%h] %ci (%cr)' $check_branch | head -n1
		_git log ${base_branch}..${check_branch} --oneline | wc -l
		echo $check_branch
	done | _row '|' pushed ancestor last commits name
}

_squash () {
    _git fetch origin $base_branch
    </dev/tty _git rebase -i $base_branch >/dev/tty 2>/dev/tty \
		|| _error failed rebasing $base_branch onto $gift_branch
	__SHLIB_PUSH_FORCE='--force'
}

_remove () {
    _git push --delete origin ${gift_branch}
    _git branch -d ${gift_branch} || _git branch -D ${gift_branch} || _error failed to delete branch $gift_branch
    _git remote update origin --prune
}

while getopts hi:f:t:PAd: arg
do
    case "$arg" in
    h) _gen_help; exit 0 ;;
    i) TYPE=ISSUE; branch="$OPTARG" ;; # issue: branch-name
    f) TYPE=FEATURE; branch="$OPTARG" ;; # feature: branch-name
    t) TYPE=TODO; branch="$OPTARG" ;; # todo: branch-name
    P) __SHLIB_GIT_PUSH=true ;; # auto push changes
    A) ARCHIVE=true ;; # turns on archive tag creation on close
    d) dfile="$OPTARG" ;; # data file
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1

: ${TYPE:=ISSUE}
: ${__SHLIB_GIT_PUSH:=false} # defaults to false anyway but explict == nice 4u <3
: ${ARCHIVE:=false}
: ${dfile:=''}
: ${branch:=bucket}
gift_branch=${TYPE}/${branch}

cd $(_git rev-parse --show-toplevel)
base_branch=$(git rev-parse --abbrev-ref HEAD)
F=$(_gift_file ${gift_branch})
DONE=true
: ${cmd:=${1:-interactive}}; test -n "$1" && shift
case $cmd in
remove) _remove ;;
list) _list ;;
show)
	for b in $(_gift_branches | grep "${1:-.*}")
	do
		echo $b
		git show ${b}:$(_gift_file ${b}) 2>/dev/null | cat # pipe to clobber autolessing
		echo 
	done
	;;

*) DONE=false ;;
esac
$DONE && return 0
_with branch $gift_branch
mkdir -p $(dirname $F)
test -f $F || { printf '# initialized %s\n' $gift_branch >$F; _git add $F ;}

case $cmd in
append) # append input to file TYPE in branch
    case "$1" in
    ''|-) cat ${dfile:--} >>$F ;;
    *) echo "$*" >>$F ;;
    esac
    _git add $F
    ;;
edit) </dev/tty $E $F >/dev/tty 2>/dev/tty ;;
merge)
    _git fetch origin
    _git merge $base_branch
	;;
squash) _squash ;;
diff) _git diff $base_branch ;;
close) # close out the gift branch
    _git merge-base --is-ancestor $gift_branch $base_branch \
		&& _error $gift_branch is an ancestor of $base_branch already 
	_squash
	_with_out branch
	# below is to force a merge on the "gift" file to use the driver
    _git merge ${gift_branch} -m "closing $gift_branch"
    $ARCHIVE || _archive
	_remove
    ;;
conf) _conf $0 $1 TYPE branch ;;
sh|interactive) # do stuff in checked out gift branch
	_shell $TYPE $branch
    ;;
*) ${cmd} "$@" ;; # say anything
esac

