# _init_test () {
# 	(cd $TD; git clone git@gitlab.com:setchell.dave/gift \
# 		|| _error failed to clone; cd gift; git submodule init
# 			git submodule update --remote)
# 	G="$PWD/gift.sh -t run-tests"
# }
_init_test () {
	(cd $TD; git clone git@gitlab.com:setchell.dave/gift \
		|| _error failed to clone)
	SH_BIN=$PWD
	SH_SHARE=$PWD/shlib
	export SH_BIN SH_SHARE
	G="$PWD/gift.sh -t run-tests"
}

_clean_test () {
	:
}

__test_list () {
	(cd $TD/gift
	$G list)
}

__test_show () {
	(cd $TD/gift
	$G show)
}

__test_append () {
	(cd $TD/gift
	$G append this and that
	$G append <<EOF
some text about
things and other than
that not much at 
all
EOF
	$G show run-tests
	$G remove)
	return 0
}

__test_diff () {
	(cd $TD/gift
	$G diff)
}
