PREFIX := ${HOME}
SH_BIN := ${PREFIX}/bin
SH_SHARE := ${PREFIX}/share
bin := gift.sh
lib := ./shlib/.*.sh

install:
	@mkdir -p ${SH_SHARE} ${SH_BIN}
	@cp -ra ${lib} ${SH_SHARE}
	@cp -ra ${bin} ${SH_BIN}
	@sed -i -e 's#@shlibdir@#${SH_SHARE}#' \
			-e 's#@bindir@#${SH_BIN}#' ${SH_BIN}/gift.sh
	@echo installed to ${PREFIX}

clean:
	@echo do some cleaning
    
.PHONY: install clean
